﻿using System.Web;
using System.Web.Mvc;

namespace EntityFramework6_DatabaseFirst_MVC5
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
